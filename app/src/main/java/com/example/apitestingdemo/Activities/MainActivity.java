package com.example.apitestingdemo.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.apitestingdemo.Controllers.DemoController;
import com.example.apitestingdemo.Interfaces.OnNetworkRequestCallback;
import com.example.apitestingdemo.Models.Comments;
import com.example.apitestingdemo.Models.PostUsers;
import com.example.apitestingdemo.Models.User;
import com.example.apitestingdemo.R;
import com.example.apitestingdemo.Services.ServiceApi;
import com.example.apitestingdemo.RequestHandler.CreateUserPostRequest;
import com.example.apitestingdemo.RequestHandler.UserPostResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity  {

    Button buttonGetData;
    Button buttonPostData;
    TextView textViewData;

    EditText editTextTitle;
    EditText editTextBody;
    EditText editTextId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        clickEvents();
        performTaskComments();
        performTaskUsers();
        performTaskGetPostUsers();

    }

    private void initViews(){

        buttonGetData = findViewById(R.id.buttonGetData);
        buttonPostData = findViewById(R.id.buttonPostData);
        textViewData = findViewById(R.id.textViewData);

        editTextBody = findViewById(R.id.editTextBody);
        editTextTitle = findViewById(R.id.editTextTitle);
        editTextId = findViewById(R.id.editTextId);

    }

    private void clickEvents(){

        buttonGetData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


               // apiResponseUsers();
               // apiPostUsersGet();



            }
        });

        buttonPostData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //apiResponsePost();
                performTaskPostResponse();
                        //editTextTitle.getText().toString(),editTextBody.getText().toString()
               // ,editTextId.getText().toString()

            }
        });

    }






    private void apiResponsePost(){

        CreateUserPostRequest createUserPostRequest = new CreateUserPostRequest(editTextTitle.getText().toString(),editTextBody.getText().toString(),editTextId.getText().toString());
        ServiceApi serviceApi = ServiceApi.retrofit.create(ServiceApi.class);
        Call<UserPostResponse> call = serviceApi.getUserPostResponse(createUserPostRequest);
        call.enqueue(new Callback<UserPostResponse>() {
            @Override
            public void onResponse(Call<UserPostResponse> call, Response<UserPostResponse> response) {

                if (response.isSuccessful()){

                        Log.d("postResponse ","data is =="+response.body());

                }

            }

            @Override
            public void onFailure(Call<UserPostResponse> call, Throwable t) {

            }
        });

    }





    private void showComments(Comments comments){
        textViewData.append(comments.getEmail()+"\n");

    }

    private void showUsers(User usersModel){
        textViewData.append(usersModel.getAddress()+"\n");

    }


    private void showPostResponse(UserPostResponse userPostResponse){
        textViewData.append(userPostResponse.getId()+"\n");

    }

    private void performTaskComments() {
        DemoController demoController = new DemoController(this);
        demoController.performTaskComments(new OnNetworkRequestCallback<List<Comments>>() {
            @Override
            public void onSuccess(List<Comments> response) {

                textViewData.append(response.toString());

                Toast.makeText(MainActivity.this, response.toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Throwable throwable) {

            }

            @Override
            public void onNetworkFailure(Throwable throwable) {

            }
        });
    }


    private void performTaskUsers(){
       DemoController demoController = new DemoController(this);
       demoController.performTaskUsers(new OnNetworkRequestCallback<List<User>>() {
           @Override
           public void onSuccess(List<User> response) {

               textViewData.append(response.toString());

               Toast.makeText(MainActivity.this, response.toString(), Toast.LENGTH_SHORT).show();

           }

           @Override
           public void onFailure(Throwable throwable) {

           }

           @Override
           public void onNetworkFailure(Throwable throwable) {

           }
       });

    }

    private void performTaskGetPostUsers(){

        DemoController demoController = new DemoController(this);
        demoController.performTaskGetPostUsers(new OnNetworkRequestCallback<List<PostUsers>>() {

            @Override
            public void onSuccess(List<PostUsers> response) {

                textViewData.append(response.toString());

                Toast.makeText(MainActivity.this, response.toString(), Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onFailure(Throwable throwable) {

            }

            @Override
            public void onNetworkFailure(Throwable throwable) {

            }
        });

    }


    private void performTaskPostResponse(){

        DemoController demoController = new DemoController(this);
        demoController.performTaskPostResponse(new OnNetworkRequestCallback<UserPostResponse>() {
            @Override
            public void onSuccess(UserPostResponse response) {

                Toast.makeText(MainActivity.this, response.toString(), Toast.LENGTH_SHORT).show();
                
                Log.d("post",""+response.toString());



            }

            @Override
            public void onFailure(Throwable throwable) {

            }

            @Override
            public void onNetworkFailure(Throwable throwable) {

            }
        });

    }


}