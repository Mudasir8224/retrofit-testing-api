package com.example.apitestingdemo.Controllers;

import android.content.Context;

import com.example.apitestingdemo.Interfaces.OnNetworkRequestCallback;
import com.example.apitestingdemo.Models.Comments;
import com.example.apitestingdemo.Models.PostUsers;
import com.example.apitestingdemo.Models.User;
import com.example.apitestingdemo.RequestHandler.CreateUserPostRequest;
import com.example.apitestingdemo.RequestHandler.UserPostResponse;
import com.example.apitestingdemo.Services.ServiceApi;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DemoController {

    private Context context;

    public DemoController(Context context) {
        this.context = context;
    }

    public void performTaskComments(final OnNetworkRequestCallback<List<Comments>> networkRequestCallback) {

        ServiceApi serviceApi = ServiceApi.retrofit.create(ServiceApi.class);
        Call<List<Comments>> call = serviceApi.getCommentData();
        call.enqueue(new Callback<List<Comments>>() {
            @Override
            public void onResponse(Call<List<Comments>> call, Response<List<Comments>> response) {
                if (response.isSuccessful()){
                    networkRequestCallback.onSuccess(response.body());
                } else {
                    networkRequestCallback.onFailure(new Throwable("Request Failure"));
                }
            }

            @Override
            public void onFailure(Call<List<Comments>> call, Throwable t) {
                networkRequestCallback.onFailure(t);
            }
        });

    }


    public void performTaskUsers(final OnNetworkRequestCallback<List<User>> networkRequestCallback){

        ServiceApi serviceApi = ServiceApi.retrofit.create(ServiceApi.class);
        Call<List<User>> call = serviceApi.getUsersData();
        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if (response.isSuccessful()){

                    networkRequestCallback.onSuccess(response.body());

                }else {
                    networkRequestCallback.onFailure(new Throwable("Request Failure"));
                }

            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {

            }
        });

    }


    public void performTaskGetPostUsers(final OnNetworkRequestCallback<List<PostUsers>> networkRequestCallback){

        ServiceApi serviceApi = ServiceApi.retrofit.create(ServiceApi.class);
        Call<List<PostUsers>> call = serviceApi.getPostUsers();
        call.enqueue(new Callback<List<PostUsers>>() {
            @Override
            public void onResponse(Call<List<PostUsers>> call, Response<List<PostUsers>> response) {

                if (response.isSuccessful()) {

                    networkRequestCallback.onSuccess(response.body());

                } else {

                    networkRequestCallback.onFailure(new Throwable());

                }

            }

            @Override
            public void onFailure(Call<List<PostUsers>> call, Throwable t) {


            }

            });

        }


        public void performTaskPostResponse(final OnNetworkRequestCallback<UserPostResponse>  networkRequestCallback){

            CreateUserPostRequest createUserPostRequest = new CreateUserPostRequest("foo","bar","1");
            ServiceApi serviceApi = ServiceApi.retrofit.create(ServiceApi.class);
            Call<UserPostResponse> call = serviceApi.getUserPostResponse(createUserPostRequest);
            call.enqueue(new Callback<UserPostResponse>() {
                @Override
                public void onResponse(Call<UserPostResponse> call, Response<UserPostResponse> response) {

                    if (response.isSuccessful()){

                        networkRequestCallback.onSuccess(response.body());

                    }else {

                        networkRequestCallback.onFailure(new Throwable());
                    }

                }

                @Override
                public void onFailure(Call<UserPostResponse> call, Throwable t) {

                }
            });



        }


    }



