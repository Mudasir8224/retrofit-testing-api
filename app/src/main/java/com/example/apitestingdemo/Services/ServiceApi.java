package com.example.apitestingdemo.Services;

import com.example.apitestingdemo.Models.Comments;
import com.example.apitestingdemo.Models.PostUsers;
import com.example.apitestingdemo.Models.User;
import com.example.apitestingdemo.RequestHandler.CreateUserPostRequest;
import com.example.apitestingdemo.RequestHandler.UserPostResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ServiceApi {

    String baseUrl = "https://jsonplaceholder.typicode.com/";

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    @GET("/posts")
    Call<List<PostUsers>> getPostUsers();

    @GET("/posts/1/comments")
    Call<List<Comments>> getCommentData();

    @GET("/users")
    Call<List<User>> getUsersData();

    @POST("posts")
    Call<UserPostResponse> getUserPostResponse(@Body CreateUserPostRequest createUserPostRequest);




}
