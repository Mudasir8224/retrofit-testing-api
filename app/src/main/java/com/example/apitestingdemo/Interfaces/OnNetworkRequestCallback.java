package com.example.apitestingdemo.Interfaces;

import retrofit2.Call;

public interface OnNetworkRequestCallback<T> {
    void onSuccess(T response);
   // void onPostSuccess(String title,String id,String body);
    void onFailure(Throwable throwable);
    void onNetworkFailure(Throwable throwable);
}
