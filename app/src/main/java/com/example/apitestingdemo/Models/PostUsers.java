package com.example.apitestingdemo.Models;

import com.google.gson.annotations.SerializedName;

public class PostUsers {

    @SerializedName("userId")
    private String userIdFirst;
    @SerializedName("id")
    private String userIdSecond;
    @SerializedName("title")
    private String titleUser;
    @SerializedName("body")
    private String bodyUser;

    public String getUserIdFirst() {
        return userIdFirst;
    }

    public void setUserIdFirst(String userIdFirst) {
        this.userIdFirst = userIdFirst;
    }

    public String getUserIdSecond() {
        return userIdSecond;
    }

    public void setUserIdSecond(String userIdSecond) {
        this.userIdSecond = userIdSecond;
    }

    public String getTitleUser() {
        return titleUser;
    }

    public void setTitleUser(String titleUser) {
        this.titleUser = titleUser;
    }

    public String getBodyUser() {
        return bodyUser;
    }

    public void setBodyUser(String bodyUser) {
        this.bodyUser = bodyUser;
    }

    @Override
    public String toString() {
        return "PostUsers{" +
                "userIdFirst='" + userIdFirst + '\'' +
                ", userIdSecond='" + userIdSecond + '\'' +
                ", titleUser='" + titleUser + '\'' +
                ", bodyUser='" + bodyUser + '\'' +
                '}';
    }
}
