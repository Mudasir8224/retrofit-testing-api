package com.example.apitestingdemo.Models;

import com.google.gson.annotations.SerializedName;

public class Address{

    @SerializedName("street")
    private String streetLahore;
    @SerializedName("suite")
    private String suiteMine;
    @SerializedName("city")
    private String cityLahore;
    @SerializedName("zipcode")
    private String zipcodeLahore;
    @SerializedName("geo")
    private Geo geoLahore;

    public String getStreetLahore() {
        return streetLahore;
    }

    public void setStreetLahore(String streetLahore) {
        this.streetLahore = streetLahore;
    }

    public String getSuiteMine() {
        return suiteMine;
    }

    public void setSuiteMine(String suiteMine) {
        this.suiteMine = suiteMine;
    }

    public String getCityLahore() {
        return cityLahore;
    }

    public void setCityLahore(String cityLahore) {
        this.cityLahore = cityLahore;
    }

    public String getZipcodeLahore() {
        return zipcodeLahore;
    }

    public void setZipcodeLahore(String zipcodeLahore) {
        this.zipcodeLahore = zipcodeLahore;
    }

    public Geo getgeoLahore() {
        return geoLahore;
    }

    public void setGeoLahore(Geo geoLahore) {
        this.geoLahore = geoLahore;
    }


    @Override
    public String toString() {
        return "Address{" +
                "streetLahore='" + streetLahore + '\'' +
                ", suiteMine='" + suiteMine + '\'' +
                ", cityLahore='" + cityLahore + '\'' +
                ", zipcodeLahore='" + zipcodeLahore + '\'' +
                ", geoLahore=" + geoLahore +
                '}';
    }
}
